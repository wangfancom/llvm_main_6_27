
func.func @argmax(%arg0 : tensor<3x2xi32>) -> (tensor<2xi32>) {
  %0 = tosa.argmax %arg0 { axis = 0 : i32} : (tensor<3x2xi32>)  -> tensor<2xi32>
  return %0 : tensor<2xi32>
}


