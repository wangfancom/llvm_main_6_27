#!/bin/bash

set -e

config_llvm() {
    TARGET=$1
    BUILD_TYPE=$2
    BUILD_DIR=$3
    OUTPUT_DIR=$4

    mkdir -p $BUILD_DIR
    pushd $BUILD_DIR

    if [ -n "$LOCAL_MAKE_ONLY" ]; then
        return
    fi
    if [ "$BUILD_TYPE" == "release" ]; then
        build_flag="-DCMAKE_BUILD_TYPE=RELEASE  -DLLVM_INSTALL_TOOLCHAIN_ONLY=ON "
        ADDITIONAL_FLAGS="-Wno-cast-function-type -Wno-stringop-truncation"
    else
        build_flag="-DCMAKE_BUILD_TYPE=DEBUG -DLLVM_ENABLE_RTTI=ON -DLLVM_ENABLE_EH=ON -DLLVM_ENABLE_ASSERTIONS=On -DLLVM_OPTIMIZED_TABLEGEN=ON "
    fi

    TARGETS="X86;AArch64;NVPTX"
    PROJS="clang;lld;mlir;"

    cmake ../llvm -G Ninja ${TOOLCHAIN_INIT} \
    -DLLVM_ENABLE_TERMINFO=OFF \
    -DLLVM_ENABLE_LIBXML2=OFF \
    -DCMAKE_INSTALL_PREFIX=$OUTPUT_DIR \
    -DLLVM_DEFAULT_TARGET_TRIPLE=$TARGET \
    -DLLVM_ENABLE_PROJECTS=$PROJS \
    -DLLVM_POLLY_LINK_INTO_TOOLS=ON \
    -DLLVM_BUILD_LLVM_DYLIB=ON \
    -DLLVM_CCACHE_BUILD=OFF \
    -DLLVM_CCACHE_DIR=.ccache  \
    -DLLVM_ENABLE_RUNTIMES=compiler-rt \
    -DLLVM_TARGETS_TO_BUILD="$TARGETS" $build_flag

    CMAKE_RET=$?

    popd
}

CMD_MODE=$1
LLVMROOT=$PWD

if [ "$CMD_MODE" == "debug" ]; then
    TARGET=x86_64-unknown-linux-gnu
    BUILD_DIR=$LLVMROOT/builddebug
    OUTPUT_DIR=$LLVMROOT/output
    BUILD_TYPE=debug
elif [ "$CMD_MODE" == "aarch64" ]; then
    # cross compile
    if [ x${CROSS_TOOLCHAIN_PATH} = x ]; then
      echo -e "\E[31m Error: CROSS_TOOLCHAIN_PATH not set."
      exit -1
    fi
    TARGET=aarch64-linux-gnu
    BUILD_DIR=$LLVMROOT/buildarm
    OUTPUT_DIR=$LLVMROOT/outputarm
    ARMROOT=${ARMROOT:-$CROSS_TOOLCHAIN_PATH}
    [ -e $ARMROOT ] || { echo -e "\E[31m Error: SYSROOT $ARMROOT not exist \E[0m"; exit -1; }
    export PATH=$ARMROOT/bin/:$PATH
    export HOST_CC=$(which gcc)
    export HOST_CXX=$(which g++)
    export CC=$ARMROOT/bin/aarch64-linux-gnu-gcc
    export CXX=$ARMROOT/bin/aarch64-linux-gnu-g++
    export CXXFLAGS="-fsigned-char"
    TOOLCHAIN_INIT="\
        -DDEFAULT_SYSROOT=$ARMROOT/aarch64-linux-gnu/libc \
        -DLLVM_TARGET_ARCH=aarch64 \
        -DLLVM_HOST_TRIPLE=aarch64-linux-gnu \
        -DLLVM_TABLEGEN=$LLVMROOT/buildrelease/bin/llvm-tblgen \
        -DCLANG_TABLEGEN=$LLVMROOT/buildrelease/bin/clang-tblgen \
        -DLLVM_CONFIG_PATH=$LLVMROOT/buildrelease/bin/llvm-config \
        -DCMAKE_SYSTEM_NAME=Linux"
    echo -e "\033[32m  build $CMD_MODE on $TARGET \n  ARMROOT=${ARMROOT} \033[0m"
    echo -e "\033[32m  BUILD_DIR=$BUILD_DIR \n  OUTPUT_DIR=$OUTPUT_DIR \n\n \033[0m"
    BUILD_TYPE=release
else # x86 release
    ARCH=$(uname -m)
    if [ "${ARCH}" == "aarch64" ]; then
        export CXXFLAGS="-fsigned-char"
        TARGET=aarch64-linux-gnu
        TOOLCHAIN_INIT="\
            -DLLVM_TARGET_ARCH=aarch64 \
            -DLLVM_HOST_TRIPLE=aarch64-linux-gnu \
            -DCMAKE_SYSTEM_NAME=Linux"
    else
        TARGET=x86_64-unknown-linux-gnu
    fi
    BUILD_DIR=$LLVMROOT/buildrelease
    OUTPUT_DIR=$LLVMROOT/output
    BUILD_TYPE=release
fi

config_llvm $TARGET $BUILD_TYPE $BUILD_DIR $OUTPUT_DIR
#cd $BUILD_DIR
#make -j16
#make install
ninja -C $BUILD_DIR -j 16 install

cp $LLVMROOT/llvm/include/llvm $OUTPUT_DIR/include -r
cp $LLVMROOT/llvm/include/llvm-c $OUTPUT_DIR/include -r
cp $BUILD_DIR/include/llvm $OUTPUT_DIR/include -r
cp $BUILD_DIR/lib/lib*.so* $OUTPUT_DIR/lib -r

